package com.gooshichand.Models;

import java.io.Serializable;

/**
 * Created by User on 3/28/2018.
 */

public class MobileModels implements Serializable {

    public String title;
    public String image;
    public String batteryCapacity;
    public String bluetooth;
    public String cameraBack;
    public String cameraFront;
    public String cpuChip;
    public String cpuCore;
    public String cpuFreq;
    public String cpuProcessor;
    public String data;
    public String dims;
    public String gallery;
    public String gpu;
    public String hasPrice;
    public String inch;
    public String link;
    public String memory;
    public String minPrice;
    public String nano;
    public String os;
    public String path;
    public String pd;
    public String priceLabel;
    public String releaseDate;
    public String reso;
    public String stbRatio;
    public String storage;
    public String stores;
    public String weight;

    public MobileModels(String title, String image, String batteryCapacity, String bluetooth,
                        String cameraBack, String cameraFront, String cpuChip, String cpuCore,
                        String cpuFreq, String cpuProcessor, String data, String dims,
                        String gallery, String gpu, String hasPrice, String inch, String link,
                        String memory, String minPrice, String nano, String os, String path,
                        String pd, String priceLabel, String releaseDate, String reso,
                        String stbRatio, String storage, String stores, String weight) {
        this.title = title;
        this.image = image;
        this.batteryCapacity = batteryCapacity;
        this.bluetooth = bluetooth;
        this.cameraBack = cameraBack;
        this.cameraFront = cameraFront;
        this.cpuChip = cpuChip;
        this.cpuCore = cpuCore;
        this.cpuFreq = cpuFreq;
        this.cpuProcessor = cpuProcessor;
        this.data = data;
        this.dims = dims;
        this.gallery = gallery;
        this.gpu = gpu;
        this.hasPrice = hasPrice;
        this.inch = inch;
        this.link = link;
        this.memory = memory;
        this.minPrice = minPrice;
        this.nano = nano;
        this.os = os;
        this.path = path;
        this.pd = pd;
        this.priceLabel = priceLabel;
        this.releaseDate = releaseDate;
        this.reso = reso;
        this.stbRatio = stbRatio;
        this.storage = storage;
        this.stores = stores;
        this.weight = weight;
    }
}
