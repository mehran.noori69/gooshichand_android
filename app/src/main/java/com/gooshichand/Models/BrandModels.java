package com.gooshichand.Models;

/**
 * Created by MaHkOoM on 3/30/2018.
 */

public class BrandModels {

    public String name;
    public String image;
    public String length;

    public BrandModels(String name, String image, String length){
        this.name = name;
        this.image = image;
        this.length = length;

    }

}
