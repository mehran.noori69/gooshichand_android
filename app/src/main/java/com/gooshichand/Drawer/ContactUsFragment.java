package com.gooshichand.Drawer;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.gooshichand.MainActivity;
import com.gooshichand.R;
import com.gooshichand.Tools.HasServiceCall;
import com.gooshichand.Tools.Preferences;
import com.gooshichand.Tools.ServiceCalls;
import com.gooshichand.Tools.Utility;

import java.util.ArrayList;

/**
 * Created by MaHkOoM on 3/29/2018.
 */

public class ContactUsFragment extends Fragment implements View.OnClickListener, HasServiceCall {

    public String serviceFunction = "safour/contact_us/";
    public String serviceType = "POST";

    public EditText sendCommentEmailText;
    public EditText sendCommentTitleText;
    public EditText sendCommentContextText;
    public ImageView sendCommentButton;
    public ProgressBar load;
    public String appTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_contact_us, container, false);
        load = (ProgressBar) rootView.findViewById(R.id.load_fragment_contactus);

        ((MainActivity)getActivity()).toggle.setVisibility(View.GONE);
//        ((MainActivity)getActivity()).searchLayout.setVisibility(View.GONE);
        appTitle = ((MainActivity)getActivity()).appTitle.getText().toString();
        ((MainActivity)getActivity()).appTitle.setText(R.string.contact_us);

        sendCommentEmailText = (EditText) rootView.findViewById(R.id.sendCommentEmailText);
        sendCommentTitleText = (EditText) rootView.findViewById(R.id.sendCommentTitleText);
        sendCommentContextText = (EditText) rootView.findViewById(R.id.sendCommentContextText);
        sendCommentButton = (ImageView) rootView.findViewById(R.id.sendCommentButton);

        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        sendCommentTitleText.requestFocus();

        adjustFonts();
        sendCommentButton.setOnClickListener(this);

        return rootView;
    }

    private ArrayList<Pair<String, String>> sendArgument(String subject, String body, String email) {
        ArrayList<Pair<String, String>> args = new ArrayList<>();
        args.add(new Pair<>("email", email));
        args.add(new Pair<>("name", subject));
        args.add(new Pair<>("content", body));
        Preferences preferences = new Preferences(this.getActivity());
        String deviceID = preferences.getString("deviceID", "false");
        args.add(new Pair<>("device_id", deviceID));
        return args;
    }

    public Boolean checkIfEmpty(){
        if (sendCommentEmailText.getText().toString().isEmpty()) {
            Utility.makeSnackBar(getActivity(), getActivity().getString(R.string.noEmail));
            return false;
        }
        if (sendCommentTitleText.getText().toString().isEmpty()) {
            Utility.makeSnackBar(getActivity(), getActivity().getString(R.string.noTitle));
            return false;
        }
        if (sendCommentContextText.getText().toString().isEmpty()) {
            Utility.makeSnackBar(getActivity(), getActivity().getString(R.string.noContext));
            return false;
        }
        return true;
    }

    private void adjustFonts() {
        Typeface myTypeface = Utility.adjustFont(this.getActivity());
        sendCommentEmailText.setTypeface(myTypeface);
        sendCommentTitleText.setTypeface(myTypeface);
        sendCommentContextText.setTypeface(myTypeface);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.sendCommentButton){
            if (checkIfEmpty()) {
                ServiceCalls serviceCalls = new ServiceCalls();
                ArrayList<Pair<String, String>> args = sendArgument(sendCommentTitleText.getText().toString(),
                        sendCommentContextText.getText().toString(), sendCommentEmailText.getText().toString());
                this.load.setVisibility(View.VISIBLE);
                serviceCalls.service(getActivity(), this, serviceFunction, serviceType, args, "");
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        ((MainActivity)getActivity()).toggle.setVisibility(View.VISIBLE);
//        ((MainActivity)getActivity()).searchLayout.setVisibility(View.VISIBLE);
        ((MainActivity)getActivity()).appTitle.setText(this.appTitle);

    }
    @Override
    public void afterServiceCall(String succeed, String hint) {
        if (succeed.contains("email is sent")){
            Utility.makeSnackBar(getActivity(), getString(R.string.email_is_sent));
            getActivity().getSupportFragmentManager().popBackStackImmediate();
        }
        this.load.setVisibility(View.GONE);

    }

    @Override
    public void afterFailedServiceCall(String failed, String hint) {

    }
}