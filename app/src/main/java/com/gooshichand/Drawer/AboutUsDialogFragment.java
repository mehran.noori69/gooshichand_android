package com.gooshichand.Drawer;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.gooshichand.R;
import com.gooshichand.Tools.Utility;

/**
 * Created by MaHkOoM on 3/29/2018.
 */

public class AboutUsDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_about_us, container, false);
        Typeface myTypeface = Utility.adjustFont(getActivity());
        TextView body = (TextView) rootView.findViewById(R.id.fragmentAboutUsBody);
        body.setTypeface(myTypeface);
        return rootView;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() == null)
            return;
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.HONEYCOMB_MR2){
            WindowManager wm = (WindowManager) getActivity().getBaseContext().
                    getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = 9 * (size.x / 10);
            int height = 2 * (size.y / 3);
            getDialog().getWindow().setLayout(width, height);
            getDialog().setCanceledOnTouchOutside(true);
        }
    }
}
