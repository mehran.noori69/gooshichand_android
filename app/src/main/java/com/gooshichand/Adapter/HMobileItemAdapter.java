package com.gooshichand.Adapter;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gooshichand.Fragment.BrandFragment;
import com.gooshichand.Fragment.MobileItemDetailesFragment;
import com.gooshichand.Models.MobileModels;
import com.gooshichand.Adapter.HMobileItemAdapter.SimpleViewHolder;
import com.gooshichand.R;
import com.gooshichand.Tools.Preferences;
import com.gooshichand.Tools.Utility;

import java.util.ArrayList;

/**
 * Created by MaHkOoM on 3/30/2018.
 */

public class HMobileItemAdapter extends RecyclerView.Adapter<SimpleViewHolder>{

    public FragmentActivity fragmentActivity;
    public Typeface myTypeface;
    public ArrayList<MobileModels> advertItems;
    public Preferences preferences;

    public HMobileItemAdapter(FragmentActivity fragmentActivity, ArrayList<MobileModels> items){
        this.fragmentActivity = fragmentActivity;
        advertItems = new ArrayList<>();
        advertItems.addAll(items);
        myTypeface = Utility.adjustFont(this.fragmentActivity);
        preferences = new Preferences(fragmentActivity);
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        public TextView listName;
        public ImageView icon;

        public SimpleViewHolder(View view) {
            super(view);
            listName = view.findViewById(R.id.h_mobile_name);
            icon = view.findViewById(R.id.h_mobile_image);
        }
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(this.fragmentActivity).inflate(R.layout.view_h_mobile_elements, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {
        holder.listName.setText(advertItems.get(position).title);
        holder.listName.setTypeface(myTypeface);

        if (advertItems.get(position).image != null && !advertItems.get(position).image.equalsIgnoreCase("")) {
            Glide
                .with(fragmentActivity)
                .load(advertItems.get(position).image)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.icon);
        }

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MobileItemDetailesFragment fragment = new MobileItemDetailesFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("item", advertItems.get(position));
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("").commit();
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return this.advertItems.size();
    }
}