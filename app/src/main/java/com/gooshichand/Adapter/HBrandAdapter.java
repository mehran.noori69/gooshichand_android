package com.gooshichand.Adapter;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gooshichand.Fragment.BrandFragment;
import com.gooshichand.Models.BrandModels;
import com.gooshichand.R;
import com.gooshichand.Tools.Preferences;
import com.gooshichand.Tools.Utility;
import com.gooshichand.Adapter.HBrandAdapter.SimpleViewHolder;

import java.util.ArrayList;

/**
 * Created by MaHkOoM on 3/30/2018.
 */

public class HBrandAdapter extends RecyclerView.Adapter<SimpleViewHolder>{

    public FragmentActivity fragmentActivity;
    public Typeface myTypeface;
    public ArrayList<BrandModels> advertItems;
    public Preferences preferences;

    public HBrandAdapter(FragmentActivity fragmentActivity, ArrayList<BrandModels> items){
        this.fragmentActivity = fragmentActivity;
        advertItems = new ArrayList<>();
        advertItems.addAll(items);
        myTypeface = Utility.adjustFont(this.fragmentActivity);
        preferences = new Preferences(fragmentActivity);
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        public TextView listName;
        public ImageView icon;

        public SimpleViewHolder(View view) {
            super(view);
            listName = view.findViewById(R.id.h_brand_name);
            icon = view.findViewById(R.id.h_brand_image);
        }
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(this.fragmentActivity).inflate(R.layout.view_h_brand_elements, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {
        holder.listName.setText(advertItems.get(position).name);
        holder.listName.setTypeface(myTypeface);

        if (advertItems.get(position).image != null && !advertItems.get(position).image.equalsIgnoreCase("")) {
            Glide
                .with(fragmentActivity)
                .load(advertItems.get(position).image)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.icon);
        }

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BrandFragment fragment = new BrandFragment();
                Bundle bundle = new Bundle();
                bundle.putString("brand", advertItems.get(position).name);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = fragmentActivity.getSupportFragmentManager();
                fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("").commit();
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return this.advertItems.size();
    }
}