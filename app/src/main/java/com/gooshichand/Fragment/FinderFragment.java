package com.gooshichand.Fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gooshichand.R;
import com.gooshichand.Tools.Utility;

/**
 * Created by MaHkOoM on 3/30/2018.
 */

public class FinderFragment  extends Fragment {

    public ProgressBar load;
    private String appTitle;
    private TextView mobileTitle;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_finder, container, false);
        Typeface myTypeface = Utility.adjustFont(getActivity());

        findViews(rootView, myTypeface);

//        this.load.setVisibility(View.VISIBLE);

        return rootView;
    }

    private void findViews(ViewGroup rootView, Typeface myTypeface) {
//        load = rootView.findViewById(R.id.mobile_detail_loading_progress);
//        mobileTitle = rootView.findViewById(R.id.mobile_detail_title);
//        mobileTitle.setTypeface(myTypeface);
    }

}
