package com.gooshichand.Fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.gooshichand.Adapter.HMobileItemAdapter;
import com.gooshichand.MainActivity;
import com.gooshichand.Models.MobileModels;
import com.gooshichand.R;
import com.gooshichand.Tools.HasServiceCall;
import com.gooshichand.Tools.Preferences;
import com.gooshichand.Tools.ServiceCalls;
import com.gooshichand.Tools.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by MaHkOoM on 3/30/2018.
 */

public class BrandFragment extends Fragment implements HasServiceCall, View.OnClickListener {

    public RecyclerView catsGridView;
    public RecyclerView newGridView;
    public RecyclerView cpuGridView;
    public RecyclerView camerabGridView;
    public RecyclerView camerafGridView;
    public RecyclerView ramGridView;
    public RecyclerView screenGridView;
    public RecyclerView weightGridView;
    public HMobileItemAdapter catsAdapter;
    public HMobileItemAdapter newAdapter;
    public HMobileItemAdapter cpuAdapter;
    public HMobileItemAdapter camerabAdapter;
    public HMobileItemAdapter camerafAdapter;
    public HMobileItemAdapter ramAdapter;
    public HMobileItemAdapter screenAdapter;
    public HMobileItemAdapter weightAdapter;
    public String serviceFunction = "brandpage/";
    public String serviceType = "POST";
    public Boolean loading;
    public int lastFirstItemPosition;
    public int currentFirstItemPosition;
    public int index;
    public ProgressBar load;
    private TextView failed;
    private PullRefreshLayout layout;
    private String brand;
    private String appTitle;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_brand, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null)
            brand = bundle.getString("brand");


        if (((MainActivity)getActivity()) != null) {
            appTitle = ((MainActivity)getActivity()).appTitle.getText().toString();
//            ((MainActivity)getActivity()).searchLayout.setVisibility(View.GONE);
            ((MainActivity)getActivity()).appTitle.setText(brand);
        }

        Typeface myTypeface = Utility.adjustFont(getActivity());

        findViews(rootView, myTypeface);

        index = 0;
        loading = false;
        lastFirstItemPosition = 0 ;
        this.load.setVisibility(View.VISIBLE);

        ArrayList<MobileModels> catsItems = new ArrayList<>();
        ArrayList<MobileModels> newItems = new ArrayList<>();
        ArrayList<MobileModels> cpuItems = new ArrayList<>();
        ArrayList<MobileModels> cbItems = new ArrayList<>();
        ArrayList<MobileModels> cfItems = new ArrayList<>();
        ArrayList<MobileModels> ramItems = new ArrayList<>();
        ArrayList<MobileModels> screenItems = new ArrayList<>();
        ArrayList<MobileModels> weightItems = new ArrayList<>();

        catsAdapter = new HMobileItemAdapter(getActivity(), catsItems);
        newAdapter = new HMobileItemAdapter(getActivity(), newItems);
        cpuAdapter = new HMobileItemAdapter(getActivity(), cpuItems);
        camerabAdapter = new HMobileItemAdapter(getActivity(), cbItems);
        camerafAdapter = new HMobileItemAdapter(getActivity(), cfItems);
        ramAdapter = new HMobileItemAdapter(getActivity(), ramItems);
        screenAdapter = new HMobileItemAdapter(getActivity(), screenItems);
        weightAdapter = new HMobileItemAdapter(getActivity(), weightItems);

        catsGridView.setAdapter(catsAdapter);
        newGridView.setAdapter(newAdapter);
        cpuGridView.setAdapter(cpuAdapter);
        camerabGridView.setAdapter(camerabAdapter);
        camerafGridView.setAdapter(camerafAdapter);
        ramGridView.setAdapter(ramAdapter);
        screenGridView.setAdapter(screenAdapter);
        weightGridView.setAdapter(weightAdapter);
        LinearLayoutManager catsGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager newGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager cpuGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager camerabGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager camerafGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager ramGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager screenGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager weightGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        catsGridView.setLayoutManager(catsGridViewLayoutManager);
        newGridView.setLayoutManager(newGridViewLayoutManager);
        cpuGridView.setLayoutManager(cpuGridViewLayoutManager);
        camerabGridView.setLayoutManager(camerabGridViewLayoutManager);
        camerafGridView.setLayoutManager(camerafGridViewLayoutManager);
        ramGridView.setLayoutManager(ramGridViewLayoutManager);
        screenGridView.setLayoutManager(screenGridViewLayoutManager);
        weightGridView.setLayoutManager(weightGridViewLayoutManager);

        Preferences preferences = new Preferences(this.getActivity());
        final String deviceID = preferences.getString("deviceID", "false");
        ArrayList<Pair<String,String>> args = new ArrayList<>();
//        args.add(new Pair<>("start", String.valueOf(index)));
        args.add(new Pair<>("brand", brand));
        args.add(new Pair<>("device_id", deviceID));
        serviceCall(this, serviceFunction, serviceType, args, "");

        layout = rootView.findViewById(R.id.horizontalSwipeRefreshLayout);
        layout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                lastFirstItemPosition = 0;
                index = 0;
                failed.setVisibility(View.GONE);

                ArrayList<Pair<String,String>> args = new ArrayList<>();
                args.add(new Pair<>("brand", brand));
                args.add(new Pair<>("device_id", deviceID));
                serviceCall(BrandFragment.this, serviceFunction, serviceType, args, "");

            }
        });

        return rootView;
    }

    private void findViews(ViewGroup rootView, Typeface myTypeface) {
        TextView catsTitle = rootView.findViewById(R.id.brand_cats_title);
        TextView newTitle =  rootView.findViewById(R.id.brand_new_title);
        TextView cpuTitle =  rootView.findViewById(R.id.brand_cpu_title);
        TextView camerabTitle =  rootView.findViewById(R.id.brand_camerab_title);
        TextView camerafTitle =  rootView.findViewById(R.id.brand_cameraf_title);
        TextView ramTitle =  rootView.findViewById(R.id.brand_ram_title);
        TextView screenTitle =  rootView.findViewById(R.id.brand_screen_title);
        TextView weightTitle =  rootView.findViewById(R.id.brand_weight_title);

        TextView catsMore = rootView.findViewById(R.id.brand_cats_more);
        TextView newMore =  rootView.findViewById(R.id.brand_new_more);
        TextView cpuMore =  rootView.findViewById(R.id.brand_cpu_more);
        TextView camerabMore =  rootView.findViewById(R.id.brand_camerab_more);
        TextView camerafMore =  rootView.findViewById(R.id.brand_cameraf_more);
        TextView ramMore =  rootView.findViewById(R.id.brand_ram_more);
        TextView screenMore =  rootView.findViewById(R.id.brand_screen_more);
        TextView weightMore =  rootView.findViewById(R.id.brand_weight_more);

        load = rootView.findViewById(R.id.grid_load_fragment_list_advertisement);
        failed = rootView.findViewById(R.id.grid_failed_list_advertisement);

        catsGridView = rootView.findViewById(R.id.brand_cats_items);
        newGridView = rootView.findViewById(R.id.brand_new_items);
        cpuGridView = rootView.findViewById(R.id.brand_cpu_items);
        camerabGridView = rootView.findViewById(R.id.brand_camerab_items);
        camerafGridView = rootView.findViewById(R.id.brand_cameraf_items);
        ramGridView = rootView.findViewById(R.id.brand_ram_items);
        screenGridView = rootView.findViewById(R.id.brand_screen_items);
        weightGridView = rootView.findViewById(R.id.brand_weight_items);

        catsTitle.setTypeface(myTypeface);
        newTitle.setTypeface(myTypeface);
        cpuTitle.setTypeface(myTypeface);
        camerabTitle.setTypeface(myTypeface);
        camerafTitle.setTypeface(myTypeface);
        ramTitle.setTypeface(myTypeface);
        screenTitle.setTypeface(myTypeface);
        weightTitle.setTypeface(myTypeface);

        catsMore.setTypeface(myTypeface);
        newMore.setTypeface(myTypeface);
        cpuMore.setTypeface(myTypeface);
        camerabMore.setTypeface(myTypeface);
        camerafMore.setTypeface(myTypeface);
        ramMore.setTypeface(myTypeface);
        screenMore.setTypeface(myTypeface);
        weightMore.setTypeface(myTypeface);

        failed.setTypeface(myTypeface);

        catsMore.setOnClickListener(this);
        newMore.setOnClickListener(this);
        cpuMore.setOnClickListener(this);
        camerabMore.setOnClickListener(this);
        camerafMore.setOnClickListener(this);
        ramMore.setOnClickListener(this);
        screenMore.setOnClickListener(this);
        weightMore.setOnClickListener(this);

    }

    public void serviceCall(BrandFragment brandFragment, String serviceFunction, String serviceType, ArrayList<Pair<String, String>> args, String hint){
        ServiceCalls serviceCalls = new ServiceCalls();
        serviceCalls.service(getActivity(), brandFragment, serviceFunction, serviceType, args, hint);
    }

    @Override
    public void afterServiceCall(String succeed, String hint) {
        JSONObject jsonObject = null;
        JSONArray jsonArray;
        try {
            jsonObject = new JSONObject(succeed);
            String cats = jsonObject.getString("cats");
            String back_camera = jsonObject.getString("back_camera");
            String front_camera = jsonObject.getString("front_camera");
            String cpu = jsonObject.getString("cpu");
            String inch = jsonObject.getString("inch");
            String newest = jsonObject.getString("newest");
            String ram = jsonObject.getString("ram");
            String weight = jsonObject.getString("weight");

            // loading cats mobile items
            ArrayList<MobileModels> catsItems = new ArrayList<>();
            Utility.parseMobileModels(cats, catsItems);
            HMobileItemAdapter hMobileItemAdapter = (HMobileItemAdapter) catsGridView.getAdapter();
            reloadMobileHAdapter(hMobileItemAdapter, catsItems);

            // loading bc mobile items
            ArrayList<MobileModels> bcItems = new ArrayList<>();
            Utility.parseMobileModels(back_camera, bcItems);
            hMobileItemAdapter = (HMobileItemAdapter) camerabGridView.getAdapter();
            reloadMobileHAdapter(hMobileItemAdapter, bcItems);

            // loading fc mobile items
            ArrayList<MobileModels> fcItems = new ArrayList<>();
            Utility.parseMobileModels(front_camera, fcItems);
            hMobileItemAdapter = (HMobileItemAdapter) camerafGridView.getAdapter();
            reloadMobileHAdapter(hMobileItemAdapter, fcItems);

            // loading cpu mobile items
            ArrayList<MobileModels> cpuItems = new ArrayList<>();
            Utility.parseMobileModels(cpu, cpuItems);
            hMobileItemAdapter = (HMobileItemAdapter) cpuGridView.getAdapter();
            reloadMobileHAdapter(hMobileItemAdapter, cpuItems);

            // loading ram mobile items
            ArrayList<MobileModels> ramItems = new ArrayList<>();
            Utility.parseMobileModels(ram, ramItems);
            hMobileItemAdapter = (HMobileItemAdapter) ramGridView.getAdapter();
            reloadMobileHAdapter(hMobileItemAdapter, ramItems);

            // loading inch mobile items
            ArrayList<MobileModels> inchItems = new ArrayList<>();
            Utility.parseMobileModels(inch, inchItems);
            hMobileItemAdapter = (HMobileItemAdapter) screenGridView.getAdapter();
            reloadMobileHAdapter(hMobileItemAdapter, inchItems);

            // loading weight mobile items
            ArrayList<MobileModels> weightItems = new ArrayList<>();
            Utility.parseMobileModels(weight, weightItems);
            hMobileItemAdapter = (HMobileItemAdapter) weightGridView.getAdapter();
            reloadMobileHAdapter(hMobileItemAdapter, weightItems);

            // loading new mobile items
            ArrayList<MobileModels> newItems = new ArrayList<>();
            Utility.parseMobileModels(newest, newItems);
            hMobileItemAdapter = (HMobileItemAdapter) newGridView.getAdapter();
            reloadMobileHAdapter(hMobileItemAdapter, newItems);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.loading = false;
        this.load.setVisibility(View.GONE);
        this.lastFirstItemPosition = this.currentFirstItemPosition;
    }

    private void reloadMobileHAdapter(HMobileItemAdapter hMobileItemAdapter, ArrayList<MobileModels> catsItems) {
        hMobileItemAdapter.advertItems.clear();
        hMobileItemAdapter.advertItems.addAll(catsItems);
        hMobileItemAdapter.notifyDataSetChanged();
        layout.setRefreshing(false);
    }

    @Override
    public void afterFailedServiceCall(String failed, String hint) {
        this.failed.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (((MainActivity)getActivity()) != null) {
            ((MainActivity)getActivity()).appTitle.setText(this.appTitle);
//        ((MainActivity)getActivity()).searchLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.brand_cats_more) {
            Bundle bundle = new Bundle();
            bundle.putString("which", "newest");
            bundle.putString("brand", brand);
            Fragment fragment = new MobileItemsFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit();
        }
        else if (v.getId() == R.id.brand_new_more) {
            Bundle bundle = new Bundle();
            bundle.putString("which", "newest");
            bundle.putString("brand", brand);
            Fragment fragment = new MobileItemsFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit();
        }
        else if (v.getId() == R.id.brand_camerab_more) {
            Bundle bundle = new Bundle();
            bundle.putString("which", "back_camera");
            bundle.putString("brand", brand);
            Fragment fragment = new MobileItemsFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit();
        }
        else if (v.getId() == R.id.brand_cameraf_more) {
            Bundle bundle = new Bundle();
            bundle.putString("which", "front_camera");
            bundle.putString("brand", brand);
            Fragment fragment = new MobileItemsFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit();
        }
        else if (v.getId() == R.id.brand_cpu_more) {
            Bundle bundle = new Bundle();
            bundle.putString("which", "cpu");
            bundle.putString("brand", brand);
            Fragment fragment = new MobileItemsFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit();
        }
        else if (v.getId() == R.id.brand_ram_more) {
            Bundle bundle = new Bundle();
            bundle.putString("which", "ram");
            bundle.putString("brand", brand);
            Fragment fragment = new MobileItemsFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit();
        }
        else if (v.getId() == R.id.brand_screen_more) {
            Bundle bundle = new Bundle();
            bundle.putString("which", "inch");
            bundle.putString("brand", brand);
            Fragment fragment = new MobileItemsFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit();
        }
        else if (v.getId() == R.id.brand_weight_more) {
            Bundle bundle = new Bundle();
            bundle.putString("which", "weight");
            bundle.putString("brand", brand);
            Fragment fragment = new MobileItemsFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit();
        }
    }
}