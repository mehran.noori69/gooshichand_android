package com.gooshichand.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.gooshichand.Adapter.HMobileItemAdapter;
import com.gooshichand.Adapter.VMobileItemAdapter;
import com.gooshichand.MainActivity;
import com.gooshichand.Models.MobileModels;
import com.gooshichand.R;
import com.gooshichand.Tools.HasServiceCall;
import com.gooshichand.Tools.ServiceCalls;
import com.gooshichand.Tools.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by MaHkOoM on 3/30/2018.
 */

public class MobileItemsFragment extends Fragment implements AbsListView.OnScrollListener, HasServiceCall {

    public VMobileItemAdapter mobileListViewAdapter;
    public RecyclerView itemListView;
    public String serviceFunction = "";
    public String serviceType = "POST";
    public Boolean loading;
    public int lastFirstItemPosition;
    public int currentFirstItemPosition;
    public int index;
    public ProgressBar load;
    public String appTitle;
    private TextView failed;
    private String section;
    private String query;
    private String brand;
    private PullRefreshLayout layout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_mobile_items, container, false);
        load = rootView.findViewById(R.id.mobile_item_load_list);
        failed = rootView.findViewById(R.id.mobile_item_failed_list);
        failed.setTypeface(Utility.adjustFont(getActivity()));

        String header = "";
        Bundle bundle = getArguments();
        if (bundle != null) {
            section = bundle.getString("which");
            if (section != null) {
                if (section.equalsIgnoreCase("search")){
                    serviceFunction = "searchitembytitle/";
                    query = bundle.getString("query");
                    header = getString(R.string.search_for) + " " + query;
//                    if ((getActivity()) != null) {
//                        ((MainActivity)getActivity()).searchLayout.setVisibility(View.GONE);
//                    }
                }else {
                    serviceFunction = "estitembytitle/";
                    brand = bundle.getString("brand");
                    header = "getmost";
                }
            }
        }
        if ((getActivity()) != null) {
            appTitle = ((MainActivity)getActivity()).appTitle.getText().toString();
            ((MainActivity)getActivity()).appTitle.setText(header);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        ArrayList<MobileModels> items = new ArrayList<>();
        itemListView = rootView.findViewById(R.id.mobile_item_list);
        mobileListViewAdapter = new VMobileItemAdapter(getActivity(), items);
        itemListView.setAdapter(mobileListViewAdapter);
        itemListView.setLayoutManager(layoutManager);

        index = 0;
        loading = false;
        lastFirstItemPosition = 0 ;

        this.load.setVisibility(View.VISIBLE);
        serviceCall(this, serviceFunction, serviceType, sendArgument(), "");

        layout = rootView.findViewById(R.id.mobile_item_swipeRefreshLayout);

        layout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                lastFirstItemPosition = 0;
                index = 0;
                failed.setVisibility(View.GONE);
                serviceCall(MobileItemsFragment.this, serviceFunction, serviceType, sendArgument(), "");
            }
        });

        return rootView;
    }

    private ArrayList<Pair<String, String>> sendArgument() {
        ArrayList<Pair<String,String>> args = new ArrayList<>();
        if(section.equalsIgnoreCase("search")) {
            args.add(new Pair<>("start", String.valueOf(index)));
            args.add(new Pair<>("search", this.query));
            args.add(new Pair<>("sort", String.valueOf(index)));
            args.add(new Pair<>("hasPrice", String.valueOf(index)));
        }
        else {
            args.add(new Pair<>("start", String.valueOf(index)));
            args.add(new Pair<>("which", section));
            args.add(new Pair<>("brand", brand));
            args.add(new Pair<>("sort", String.valueOf(index)));
            args.add(new Pair<>("hasPrice", String.valueOf(index)));
        }
        return args;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        currentFirstItemPosition = firstVisibleItem;
        if(lastFirstItemPosition<firstVisibleItem && !loading && totalItemCount!=0 && firstVisibleItem+visibleItemCount>=totalItemCount){
            loading = true;
            this.load.setVisibility(View.VISIBLE);
            serviceCall(this, serviceFunction, serviceType, sendArgument(), "");
        }
    }

    public void serviceCall(MobileItemsFragment mobileItemsFragment, String serviceFunction, String serviceType, ArrayList<Pair<String, String>> args, String hint){
        ServiceCalls serviceCalls = new ServiceCalls();
        serviceCalls.service(getActivity(), mobileItemsFragment, serviceFunction, serviceType, args, hint);
        index += Utility.paginationSize;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if ((getActivity()) != null) {
            ((MainActivity)getActivity()).appTitle.setText(this.appTitle);
//            ((MainActivity)getActivity()).searchLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void afterServiceCall(String succeed, String hint) {
        ArrayList<MobileModels> catsItems = new ArrayList<>();
        Utility.parseMobileModels(succeed, catsItems);
        VMobileItemAdapter vMobileItemAdapter = (VMobileItemAdapter) itemListView.getAdapter();
        reloadMobileHAdapter(vMobileItemAdapter, catsItems);

        this.loading = false;
        this.load.setVisibility(View.GONE);
        this.lastFirstItemPosition = this.currentFirstItemPosition;
    }

    private void reloadMobileHAdapter(VMobileItemAdapter vMobileItemAdapter, ArrayList<MobileModels> catsItems) {
        vMobileItemAdapter.advertItems.clear();
        vMobileItemAdapter.advertItems.addAll(catsItems);
        vMobileItemAdapter.notifyDataSetChanged();
        layout.setRefreshing(false);
    }

    @Override
    public void afterFailedServiceCall(String failed, String hint) {
        this.failed.setVisibility(View.VISIBLE);
    }
}