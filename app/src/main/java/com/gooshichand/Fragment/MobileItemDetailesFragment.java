package com.gooshichand.Fragment;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.gooshichand.MainActivity;
import com.gooshichand.Models.MobileModels;
import com.gooshichand.R;
import com.gooshichand.Tools.HasServiceCall;
import com.gooshichand.Tools.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by MaHkOoM on 3/30/2018.
 */

public class MobileItemDetailesFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, HasServiceCall {

    public ProgressBar load;
    private String appTitle;
    private TextView mobileTitle;
    private TextView mobileOS;
    private TextView mobileReleaseDate;
    private TextView mobileBackCamera;
    private TextView mobileFrontCamera;
    private TextView mobileDim;
    private TextView mobileWeight;
    private TextView mobileInch;
    private TextView mobilePd;
    private TextView mobileReso;
    private TextView mobileStbRatio;
    private TextView mobileMemory;
    private TextView mobileGpu;
    private TextView mobileData;
    private TextView mobileNano;
    private TextView mobileBluetooth;
    private TextView mobileBatteryCap;
    private TextView mobileCpu;
    private TextView mobileCpuCore;
    private TextView mobileCpuFreq;
    private SliderLayout mDemoSlider;
    private MobileModels mobileModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_mobile_detailes, container, false);
        Typeface myTypeface = Utility.adjustFont(getActivity());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mobileModel = (MobileModels)bundle.getSerializable("item");
            if ((getActivity()) != null) {
                appTitle = ((MainActivity)getActivity()).appTitle.getText().toString();
//                ((MainActivity)getActivity()).searchLayout.setVisibility(View.GONE);
                if (mobileModel != null) {
                    ((MainActivity)getActivity()).appTitle.setText(mobileModel.title);
                }
            }
        }

        findViews(rootView, myTypeface);
        initialValues();

        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);
        String[] galleries = mobileModel.gallery.split(",");
        for(String gallery: galleries){
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            defaultSliderView .image(gallery)
                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(this);
            mDemoSlider.addSlider(defaultSliderView);
        }

        this.load.setVisibility(View.VISIBLE);

        return rootView;
    }

    @SuppressLint("ResourceType")
    private void initialValues() {
        mobileTitle.setText(String.format("%s : %s", getString(R.string.mobile_title), mobileModel.title));
        mobileOS.setText(String.format("%s : %s", getString(R.string.mobile_os), mobileModel.os));
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-mm-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date date=formatter.parse(mobileModel.releaseDate);
            mobileReleaseDate.setText(String.format("%s : %s - %s", getString(R.string.mobile_release_date)
                    , date.toGMTString().split(" ")[1], date.toGMTString().split(" ")[2]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mobileBackCamera.setText(String.format("%s : %s %s", getString(R.string.mobile_back_camera), mobileModel.cameraBack, getString(R.string.megapixel)));
        mobileFrontCamera.setText(String.format("%s : %s %s", getString(R.string.mobile_back_camera), mobileModel.cameraFront, getString(R.string.megapixel)));
    }

    private void findViews(ViewGroup rootView, Typeface myTypeface) {
        mDemoSlider = rootView.findViewById(R.id.mobile_detail_slider);
        load = rootView.findViewById(R.id.mobile_detail_loading_progress);
        mobileTitle = rootView.findViewById(R.id.mobile_detail_title);
        mobileOS = rootView.findViewById(R.id.mobile_detail_os);
        mobileReleaseDate = rootView.findViewById(R.id.mobile_detail_release);
        mobileBackCamera = rootView.findViewById(R.id.mobile_detail_bc);
        mobileFrontCamera = rootView.findViewById(R.id.mobile_detail_fc);
        mobileDim = rootView.findViewById(R.id.mobile_detail_dim);
        mobileWeight = rootView.findViewById(R.id.mobile_detail_weight);
        mobileInch = rootView.findViewById(R.id.mobile_detail_inch);
        mobilePd = rootView.findViewById(R.id.mobile_detail_pd);
        mobileReso = rootView.findViewById(R.id.mobile_detail_reso);
        mobileStbRatio = rootView.findViewById(R.id.mobile_detail_stbratio);
        mobileMemory = rootView.findViewById(R.id.mobile_detail_memory);
        mobileGpu = rootView.findViewById(R.id.mobile_detail_gpu);
        mobileData = rootView.findViewById(R.id.mobile_detail_data);
        mobileNano = rootView.findViewById(R.id.mobile_detail_nano);
        mobileBluetooth = rootView.findViewById(R.id.mobile_detail_bluetooth);
        mobileBatteryCap = rootView.findViewById(R.id.mobile_detail_battery_cap);
        mobileCpu = rootView.findViewById(R.id.mobile_detail_cpu);
        mobileCpuCore = rootView.findViewById(R.id.mobile_detail_cpu_core);
        mobileCpuFreq = rootView.findViewById(R.id.mobile_detail_cpu_freq);
        mobileTitle.setTypeface(myTypeface);
        mobileOS.setTypeface(myTypeface);
        mobileReleaseDate.setTypeface(myTypeface);
        mobileBackCamera.setTypeface(myTypeface);
        mobileFrontCamera.setTypeface(myTypeface);
        mobileDim.setTypeface(myTypeface);
        mobileWeight.setTypeface(myTypeface);
        mobileInch.setTypeface(myTypeface);
        mobilePd.setTypeface(myTypeface);
        mobileReso.setTypeface(myTypeface);
        mobileStbRatio.setTypeface(myTypeface);
        mobileMemory.setTypeface(myTypeface);
        mobileGpu.setTypeface(myTypeface);
        mobileData.setTypeface(myTypeface);
        mobileNano.setTypeface(myTypeface);
        mobileBluetooth.setTypeface(myTypeface);
        mobileBatteryCap.setTypeface(myTypeface);
        mobileCpu.setTypeface(myTypeface);
        mobileCpuCore.setTypeface(myTypeface);
        mobileCpuFreq.setTypeface(myTypeface);
    }

    @Override
    public void afterServiceCall(String succeed, String hint) {
    }

    @Override
    public void afterFailedServiceCall(String failed, String hint) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if ((getActivity()) != null) {
            ((MainActivity)getActivity()).appTitle.setText(this.appTitle);
//            if(getActivity().getSupportFragmentManager().getBackStackEntryCount()<= 1)
//                ((MainActivity)getActivity()).searchLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}