package com.gooshichand.Fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.gooshichand.Adapter.HBrandAdapter;
import com.gooshichand.Adapter.HMobileItemAdapter;
import com.gooshichand.Models.BrandModels;
import com.gooshichand.Models.MobileModels;
import com.gooshichand.R;
import com.gooshichand.Tools.HasServiceCall;
import com.gooshichand.Tools.Preferences;
import com.gooshichand.Tools.ServiceCalls;
import com.gooshichand.Tools.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by MaHkOoM on 3/30/2018.
 */

public class HomeFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, HasServiceCall, View.OnClickListener {

    public LinearLayout searchLayout;
    public ImageView searchButton ;
    public EditText searchText;
    public RecyclerView catsGridView;
    public RecyclerView recomGridView;
    public HBrandAdapter catsAdapter;
    public HMobileItemAdapter recomAdapter;
    public String serviceFunction = "homepage/";
    public String serviceType = "POST";
    public Boolean loading;
    public int lastFirstItemPosition;
    public int currentFirstItemPosition;
    public int index;
    public ProgressBar load;
    private TextView failed;
    private PullRefreshLayout layout;
    private HashMap<String, String>  url_maps;
    private SliderLayout mDemoSlider;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_home, container, false);
        Typeface myTypeface = Utility.adjustFont(getActivity());
        TextView catsTitle = rootView.findViewById(R.id.home_cats_title);
        catsTitle.setTypeface(myTypeface);
        TextView recomTitle =  rootView.findViewById(R.id.home_recom_title);
        recomTitle.setTypeface(myTypeface);
        LinearLayout recomLL = rootView.findViewById(R.id.home_recom_ll);

        searchButton = rootView.findViewById(R.id.search_icon_in_app);
        searchButton.setOnClickListener(this);
        searchText = rootView.findViewById(R.id.search_text_in_app);
        searchText.setTypeface(myTypeface);
        searchLayout = rootView.findViewById(R.id.searchLayout);

        load = rootView.findViewById(R.id.grid_load_fragment_list_advertisement);
        failed = rootView.findViewById(R.id.grid_failed_list_advertisement);
        failed.setTypeface(myTypeface);

        index = 0;
        loading = false;
        lastFirstItemPosition = 0 ;
        this.load.setVisibility(View.VISIBLE);

        mDemoSlider = rootView.findViewById(R.id.slider);
        url_maps = new HashMap<>();
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);

        LinearLayoutManager catsGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        ArrayList<BrandModels> catsItems = new ArrayList<>();
        catsGridView = rootView.findViewById(R.id.home_cats_items);
        catsAdapter = new HBrandAdapter(getActivity(), catsItems);
        catsGridView.setAdapter(catsAdapter);
        catsGridView.setLayoutManager(catsGridViewLayoutManager);

        LinearLayoutManager recomGridViewLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        ArrayList<MobileModels> recomItems = new ArrayList<>();
        recomGridView = rootView.findViewById(R.id.home_recom_items);
        recomAdapter = new HMobileItemAdapter(getActivity(), recomItems);
        recomGridView.setAdapter(recomAdapter);
        recomGridView.setLayoutManager(recomGridViewLayoutManager);

        recomTitle.setText(Utility.offerName);
        if(Utility.hasOffer){
            recomTitle.setVisibility(View.VISIBLE);
            recomGridView.setVisibility(View.VISIBLE);
            recomLL.setVisibility(View.VISIBLE);
        }else {
            recomTitle.setVisibility(View.GONE);
            recomGridView.setVisibility(View.GONE);
            recomLL.setVisibility(View.GONE);
        }

        Preferences preferences = new Preferences(this.getActivity());
        final String deviceID = preferences.getString("deviceID", "false");
        ArrayList<Pair<String,String>> args = new ArrayList<>();
        args.add(new Pair<>("device_id", deviceID));
        serviceCall(this, serviceFunction, serviceType, args, "");

        layout = rootView.findViewById(R.id.horizontalSwipeRefreshLayout);
        layout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                lastFirstItemPosition = 0;
                index = 0;
                failed.setVisibility(View.GONE);

                ArrayList<Pair<String,String>> args = new ArrayList<>();
                args.add(new Pair<>("device_id", deviceID));
                serviceCall(HomeFragment.this, serviceFunction, serviceType, args, "");

            }
        });

        return rootView;
    }

    public void serviceCall(HomeFragment homeFragment, String serviceFunction, String serviceType, ArrayList<Pair<String, String>> args, String hint){
        ServiceCalls serviceCalls = new ServiceCalls();
        serviceCalls.service(getActivity(), homeFragment, serviceFunction, serviceType, args, hint);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void afterServiceCall(String succeed, String hint) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(succeed);
            String homeSlideImages = jsonObject.getString("home_slide_images");
            String mobileBrands = jsonObject.getString("mobile_brands");
            String mobileRecommanded = jsonObject.getString("mobile_recommanded");

            // loading image slider data
            JSONArray jsonArray = new JSONArray(homeSlideImages);
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);
                url_maps.put(String.valueOf(i), jsonObject.getString("link"));
            }
            for(String name : url_maps.keySet()){
                DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
                defaultSliderView .image(url_maps.get(name))
                        .setScaleType(BaseSliderView.ScaleType.Fit)
                        .setOnSliderClickListener(this);
                mDemoSlider.addSlider(defaultSliderView);
            }

            // loading mobile category data
            ArrayList<BrandModels> catsItems = new ArrayList<>();
            Utility.parseMobilebrands(mobileBrands, catsItems);
            HBrandAdapter hBrandAdapter = (HBrandAdapter) catsGridView.getAdapter();
            reloadBrandAdapter(hBrandAdapter, catsItems);

            // loading recommended mobile items
            ArrayList<MobileModels> mobileItems = new ArrayList<>();
            Utility.parseMobileModels(mobileRecommanded, mobileItems);
            HMobileItemAdapter hMobileItemAdapter = (HMobileItemAdapter) recomGridView.getAdapter();
            reloadMobileHAdapter(hMobileItemAdapter, mobileItems);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.loading = false;
        this.load.setVisibility(View.GONE);
        this.lastFirstItemPosition = this.currentFirstItemPosition;
    }

    @Override
    public void afterFailedServiceCall(String failed, String hint) {
        this.failed.setVisibility(View.VISIBLE);
    }

    private void reloadBrandAdapter(HBrandAdapter hBrandAdapter, ArrayList<BrandModels> catsItems) {
        hBrandAdapter.advertItems.clear();
        hBrandAdapter.advertItems.addAll(catsItems);
        hBrandAdapter.notifyDataSetChanged();
        layout.setRefreshing(false);
    }

    private void reloadMobileHAdapter(HMobileItemAdapter hMobileItemAdapter, ArrayList<MobileModels> catsItems) {
        hMobileItemAdapter.advertItems.clear();
        hMobileItemAdapter.advertItems.addAll(catsItems);
        hMobileItemAdapter.notifyDataSetChanged();
        layout.setRefreshing(false);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.search_icon_in_app){
            Fragment fragment = null;
            Bundle bundle = new Bundle();
            bundle.putString("which", "search");
            bundle.putString("query", searchText.getText().toString());
            fragment = new MobileItemsFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("fragment").commit();
        }

    }
}
