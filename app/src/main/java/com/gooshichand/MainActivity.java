package com.gooshichand;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gooshichand.Drawer.AboutUsDialogFragment;
import com.gooshichand.Drawer.ContactUsFragment;
import com.gooshichand.Fragment.CompareFragment;
import com.gooshichand.Fragment.FinderFragment;
import com.gooshichand.Fragment.HomeFragment;
import com.gooshichand.Fragment.MobileItemsFragment;
import com.gooshichand.Tools.Preferences;
import com.gooshichand.Tools.UpdateDialogFragment;
import com.gooshichand.Tools.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class MainActivity extends FragmentActivity
        implements View.OnClickListener, FragmentManager.OnBackStackChangedListener {

    public DrawerLayout drawer;
    public NavigationView navigationView;
    public Boolean isReadyToExit;
    public ImageView firstImage;
    public TextView firstText;
    public ImageView secondImage;
    public TextView secondText;
    public ImageView thirdImage;
    public TextView thirdText;
    public ImageView backButton;
    public TextView appTitle;
    public ImageView toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Typeface myTypeface = Utility.adjustFont(this);
        this.drawer = findViewById(R.id.drawer_layout);
        this.navigationView = findViewById(R.id.nav_view);

        prepareDrawble(myTypeface);
        registerDevice();
        changeActionBar(myTypeface);
        loadFragment();
        checkVersion();

        firstImage.requestFocus();

        this.isReadyToExit = false;
    }

    private void prepareDrawble(Typeface myTypeface) {
        LinearLayout contactUs = navigationView.findViewById(R.id.nav_contact_us);
        contactUs.setOnClickListener(this);
        TextView contactUsTxt = navigationView.findViewById(R.id.nav_contact_us_txt);
        contactUsTxt.setTypeface(myTypeface);
        LinearLayout shareApp = navigationView.findViewById(R.id.nav_share);
        shareApp.setOnClickListener(this);
        TextView shareAppTxt = navigationView.findViewById(R.id.nav_share_txt);
        shareAppTxt.setTypeface(myTypeface);
        LinearLayout aboutUs = navigationView.findViewById(R.id.nav_about_us);
        aboutUs.setOnClickListener(this);
        TextView aboutUsTxt = navigationView.findViewById(R.id.nav_about_us_txt);
        aboutUsTxt.setTypeface(myTypeface);
    }

    private void checkVersion() {
        String baseUrl = getString(R.string.serviceBaseURL);
        String serviceFunction = "getversion/";
        String serviceURL = baseUrl + serviceFunction ;
        String result;
        HttpGetRequest getRequest = new HttpGetRequest();
        try {
            result = getRequest.execute(serviceURL).get();
            try {
                if(result != null) {
                    JSONObject jsonObject = new JSONObject(result);
                    String versioncode = jsonObject.getString("versioncode");
                    String minversioncode = jsonObject.getString("minversioncode");
                    String updatetext = jsonObject.getString("updatetext");
                    String versionname = jsonObject.getString("versionname");
                    String versionurl = jsonObject.getString("versionUrl");
                    String has_offer = jsonObject.getString("has_offer");
                    String offer_name = jsonObject.getString("offer_name");
                    if (has_offer.equalsIgnoreCase("true"))
                        Utility.hasOffer = true;
                    else
                        Utility.hasOffer = false;
                    Utility.offerName = offer_name;

                    int serverMinVersionCode = Integer.valueOf(minversioncode);
                    int serverVersionCode = Integer.valueOf(versioncode);

                    PackageInfo pInfo = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    int clientVersionCode = 0;
                    if (pInfo != null) {
                        clientVersionCode = pInfo.versionCode;
                    }

                    if (serverMinVersionCode > clientVersionCode) { // Force Update
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        UpdateDialogFragment dialogFragment = new UpdateDialogFragment();
                        dialogFragment.comment = updatetext;
                        dialogFragment.link = versionurl;
                        dialogFragment.show(fragmentManager, "Dialog Fragment");
                    } else if (serverMinVersionCode <= clientVersionCode && serverVersionCode > clientVersionCode) { // Optional Update
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        UpdateDialogFragment dialogFragment = new UpdateDialogFragment();
                        dialogFragment.comment = updatetext;
                        dialogFragment.link = versionurl;
                        dialogFragment.show(fragmentManager, "Dialog Fragment");
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    private void loadFragment() {
        HomeFragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);
        while (fragmentManager.getBackStackEntryCount() > 0)
            fragmentManager.popBackStackImmediate();
        fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("").commit();
    }

    private void changeActionBar(Typeface myTypeface) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
        appTitle = findViewById(R.id.slidingMenuTitle);
        appTitle.setTypeface(myTypeface);
        toggle = findViewById(R.id.slideMenuButton);
        toggle.setOnClickListener(this);
//        searchButton = findViewById(R.id.search_icon_in_app);
//        searchButton.setOnClickListener(this);
//        searchText = findViewById(R.id.search_text_in_app);
//        searchText.setTypeface(myTypeface);
        firstImage = findViewById(R.id.firstImage);
        firstImage.setOnClickListener(this);
        secondImage = findViewById(R.id.secondImage);
        secondImage.setOnClickListener(this);
        thirdImage = findViewById(R.id.thirdImage);
        thirdImage.setOnClickListener(this);
        firstText = findViewById(R.id.firstText);
        firstText.setTypeface(myTypeface);
        firstText.setOnClickListener(this);
        secondText = findViewById(R.id.secondText);
        secondText.setTypeface(myTypeface);
        secondText.setOnClickListener(this);
        thirdText = findViewById(R.id.thirdText);
        thirdText.setTypeface(myTypeface);
        thirdText.setOnClickListener(this);
        backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(this);

        this.firstImage.setClickable(false);
        this.firstText.setClickable(false);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(getSupportFragmentManager().getBackStackEntryCount()>1) {
            getSupportFragmentManager().popBackStackImmediate();
            return;
        }
        if(!isReadyToExit){
            Utility.makeSnackBar(this, getString(R.string.clickBackTwice));
            isReadyToExit=!isReadyToExit;
            return;
        }
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.slideMenuButton) {
            if (drawer.isDrawerVisible(GravityCompat.START))
                drawer.closeDrawer(drawer);
            else
                drawer.openDrawer(GravityCompat.START);
        }
        else if (v.getId() == R.id.nav_contact_us) {
            ContactUsFragment fragment = new ContactUsFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment, "contactus").addToBackStack("fragment").commit();
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (v.getId() == R.id.nav_about_us) {
            AboutUsDialogFragment dialogFragment = new AboutUsDialogFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            dialogFragment.show(fragmentManager, "Dialog Fragment");
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (v.getId() == R.id.nav_share) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing_drawer_post_body));
            startActivity(Intent.createChooser(shareIntent, getString(R.string.sharing_drawer_post_title)));
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(v.getId()==R.id.backButton){
            getSupportFragmentManager().popBackStackImmediate();
        }
        else if(v.getId()==R.id.firstImage || v.getId()==R.id.firstText){
            makeMenuItem(true, false, false);
            Fragment fragment = new HomeFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            while (fragmentManager.getBackStackEntryCount() > 0)
                fragmentManager.popBackStackImmediate();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("").commit();
        }
        else if(v.getId()==R.id.secondImage || v.getId()==R.id.secondText){
            makeMenuItem(false, true, false);
            Fragment fragment = new FinderFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            while (fragmentManager.getBackStackEntryCount() > 0)
                fragmentManager.popBackStackImmediate();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("").commit();
        }
        else if(v.getId()==R.id.thirdImage || v.getId()==R.id.thirdText){
            makeMenuItem(false, false, true);
            Fragment fragment = new CompareFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            while (fragmentManager.getBackStackEntryCount() > 0)
                fragmentManager.popBackStackImmediate();
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).addToBackStack("").commit();
        }
    }

    private void makeMenuItem(Boolean first, Boolean second, Boolean third) {
        if (first){
            this.firstImage.setImageResource(R.drawable.about_us);
            this.firstText.setTextColor(getResources().getColor(R.color.Orange));
            this.firstImage.setClickable(false);
            this.firstText.setClickable(false);
        }else {
            this.firstImage.setImageResource(R.drawable.about_us);
            this.firstText.setTextColor(getResources().getColor(R.color.Gray));
            this.firstImage.setClickable(true);
            this.firstText.setClickable(true);
        }
        if (second){
            this.secondImage.setImageResource(R.drawable.about_us);
            this.secondText.setTextColor(getResources().getColor(R.color.Orange));
            this.secondImage.setClickable(false);
            this.secondText.setClickable(false);
        }else {
            this.secondImage.setImageResource(R.drawable.about_us);
            this.secondText.setTextColor(getResources().getColor(R.color.Gray));
            this.secondImage.setClickable(true);
            this.secondText.setClickable(true);
        }
        if (third){
            this.thirdImage.setImageResource(R.drawable.about_us);
            this.thirdText.setTextColor(getResources().getColor(R.color.Orange));
            this.thirdImage.setClickable(false);
            this.thirdText.setClickable(false);
        }else {
            this.thirdImage.setImageResource(R.drawable.about_us);
            this.thirdText.setTextColor(getResources().getColor(R.color.Gray));
            this.thirdImage.setClickable(true);
            this.thirdText.setClickable(true);
        }
    }

    @Override
    public void onBackStackChanged() {
        if(getSupportFragmentManager().getBackStackEntryCount()>=2) {
            backButton.setVisibility(View.VISIBLE);
        }else if(getSupportFragmentManager().getBackStackEntryCount()<2) {
            backButton.setVisibility(View.GONE);
        }
    }

    private void registerDevice() {
        Preferences preferences = new Preferences(this);
//        preferences.removeString("deviceID");
        String deviceID = preferences.getString("deviceID", "false");
        if(deviceID.equalsIgnoreCase("false")){
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date currentDate = new Date();
            dateFormat.format(currentDate);
            preferences.saveString("deviceID",currentDate.toString());
        }
//        deviceID = preferences.getString("deviceID", "false");
//        Log.e("salam", deviceID);
    }

    public class HttpGetRequest extends AsyncTask<String, Void, String> {
        static final String REQUEST_METHOD = "GET";
        static final int READ_TIMEOUT = 15000;
        static final int CONNECTION_TIMEOUT = 15000;
        @Override
        protected String doInBackground(String... params){
            String stringUrl = params[0];
            String result;
            String inputLine;
            try {
                URL myUrl = new URL(stringUrl);
                HttpURLConnection connection =(HttpURLConnection)
                        myUrl.openConnection();
                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);

                connection.connect();
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();
                while((inputLine = reader.readLine()) != null){
                    stringBuilder.append(inputLine);
                }
                reader.close();
                streamReader.close();
                result = stringBuilder.toString();
            }
            catch(IOException e){
                e.printStackTrace();
                result = null;
            }
            return result;
        }
        protected void onPostExecute(String result){
            super.onPostExecute(result);
        }
    }

}
