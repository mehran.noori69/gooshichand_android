package com.gooshichand.Tools;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.CursorLoader;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gooshichand.Models.BrandModels;
import com.gooshichand.Models.MobileModels;
import com.gooshichand.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;



/**
 * Created by MaHkOoM on 10/20/2015.
 */
public class Utility {

    public static ImageView imageView;
    public static Boolean isCropped;
    public static int paginationSize = 40;
    public static Typeface myTypeface = null;
    public static Boolean hasOffer = false;
    public static String offerName = null;

    public static void parseMobilebrands(String response, ArrayList<BrandModels> items){
        if(response.length()>0) {
            try {
                JSONArray jsonArray = new JSONArray(response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    BrandModels cm = new BrandModels(jsonObject.getString("name"),
                            jsonObject.getString("img"), jsonObject.getString("specs_length"));
                    items.add(cm);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void parseMobileModels(String response, ArrayList<MobileModels> items){
        if(response.length()>0) {
            try {
                JSONArray jsonArray = new JSONArray(response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    MobileModels cm = new MobileModels(jsonObject.getString("title"),
                            jsonObject.getString("img"), jsonObject.getString("battery_capacity"),
                            jsonObject.getString("bluetooth"), jsonObject.getString("camera_back"),
                            jsonObject.getString("camera_front"), jsonObject.getString("cpu_chip"),
                            jsonObject.getString("cpu_core"), jsonObject.getString("cpu_freq"),
                            jsonObject.getString("cpu_processor"), jsonObject.getString("data"),
                            jsonObject.getString("dims"), jsonObject.getString("gallery"),
                            jsonObject.getString("gpu"), jsonObject.getString("hasprice"),
                            jsonObject.getString("inch"), jsonObject.getString("link"),
                            jsonObject.getString("memory"), jsonObject.getString("minprice"),
                            jsonObject.getString("nano"), jsonObject.getString("os"),
                            jsonObject.getString("path"), jsonObject.getString("pd"),
                            jsonObject.getString("price_label"), jsonObject.getString("release_date"),
                            jsonObject.getString("reso"), jsonObject.getString("stbratio"),
                            jsonObject.getString("storage"), jsonObject.getString("stores"),
                            jsonObject.getString("weight"));
                    items.add(cm);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void showToast(Context context, String error, int size){
        Toast toast = Toast.makeText(context, error, Toast.LENGTH_SHORT);
        LinearLayout toastLayout = (LinearLayout) toast.getView();
        TextView toastTV = (TextView) toastLayout.getChildAt(0);
        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.font));
        toastTV.setTypeface(myTypeface);
        toastTV.setTextSize(size);
        toast.show();
    }

    public static Typeface adjustFont(FragmentActivity fragmentActivity){
        if(myTypeface==null)
            myTypeface = Typeface.createFromAsset(fragmentActivity.getAssets(), fragmentActivity.getString(R.string.font));
        return myTypeface;
    }

    public static String getApplicationName(Context context) {
        int stringId = context.getApplicationInfo().labelRes;
        return context.getString(stringId);
    }

    public static String loadJSONFromAsset(FragmentActivity fragmentActivity, String fileName) {
        String json = null;
        try {
            InputStream is = fragmentActivity.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String toPersianNumber(String input){
        String[] persian = { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };
        for (int j=0; j<persian.length; j++)
            input = input.replace(String.valueOf(j), persian[j]);

        return input;
    }

    public static String toEnglishNumber(String input){
        char[] chars = new char[input.length()];
        for(int i=0;i<input.length();i++) {
            char ch = input.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        return new String(chars);
    }

    public static Boolean isValidDates(String dateFrom, String dateTill){
        if (dateFrom==null || dateFrom.equalsIgnoreCase(""))
            return false;
        String []tempFrom = dateFrom.split("/");
        String []tempTill = dateTill.split("/");

        if ( tempTill[0]!="" && Integer.valueOf(tempFrom[0]) < Integer.valueOf(tempTill[0]) )
            return true;
        else if (  tempTill[0]!="" && Integer.valueOf(tempFrom[0]) > Integer.valueOf(tempTill[0]) )
            return false;
        else {
            if ( tempTill[1]!="" && Integer.valueOf(tempFrom[1]) < Integer.valueOf(tempTill[1]))
                return true;
            else if (  tempTill[1]!="" && Integer.valueOf(tempFrom[1]) > Integer.valueOf(tempTill[1]) )
                return false;
            else {
                if ( tempTill[2]!="" && Integer.valueOf(tempFrom[2]) <= Integer.valueOf(tempTill[2]))
                    return true;
                else if (  tempTill[2]!="" && Integer.valueOf(tempFrom[2]) > Integer.valueOf(tempTill[2]) )
                    return false;
            }
        }

        return true;
    }

    public static void takePhoto(final Fragment fragment, final int selectFile, ImageView imgPreview){
        imageView = imgPreview;
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        fragment.startActivityForResult(Intent.createChooser(intent, "Select File"), selectFile);
    }

    public static Bitmap onResult(Fragment fragment, Intent data){
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        CursorLoader cursorLoader = new CursorLoader(fragment.getActivity(),selectedImageUri, projection, null, null,
                null);
        Cursor cursor =cursorLoader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        String selectedImagePath = cursor.getString(column_index);
        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 400;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);
        imageView.setImageBitmap(bm);
        return bm;
    }

    public static Bitmap createThumbnail(Bitmap imageBitmap){
        final int THUMBNAIL_SIZE = 200;
        return ThumbnailUtils.extractThumbnail(imageBitmap, THUMBNAIL_SIZE, THUMBNAIL_SIZE);
    }

    public static void performCrop(Fragment fragment, Uri picUri, int requestCode) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(picUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 4);
            cropIntent.putExtra("aspectY", 3);
            cropIntent.putExtra("outputX", 640);
            cropIntent.putExtra("outputY", 480);
            cropIntent.putExtra("return-data", true);
            Utility.isCropped = true;
            fragment.startActivityForResult(cropIntent, requestCode);
        }
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast.makeText(fragment.getActivity(), "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public static String encodeToBase64(Bitmap image){
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public static Bitmap decodeBase64(String input){
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static String changeTimeZoneToIran(String time) throws ParseException {
        SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date temp = utcFormat.parse(time);
        SimpleDateFormat persianFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        persianFormat.setTimeZone(TimeZone.getTimeZone("Iran"));
        time = persianFormat.format(temp);
        return time;
    }

    public static String georgianToJalali(String time) throws ParseException {
        if(time.contains(".")){
            time = changeTimeZoneToIran(time);
        }
        String tempTime = time.substring(time.indexOf('T') + 1, time.length());
        String []timeArray = tempTime.split(":");
        String tempDate = time.substring(0, time.indexOf('T'));
        String []dateArray = tempDate.split("-");
        String day = dateArray[2];
        String month = dateArray[1];
        String year = dateArray[0];
        String minute = timeArray[1];
        String hour = timeArray[0];
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        Date newDate = sdf.parse(day+"/"+month+"/"+year+" "+hour+":"+minute);
        JalaliCalendar jalaliCalendar = new JalaliCalendar();
        time = jalaliCalendar.getJalaliDate(newDate) +" "+hour+":"+minute;
        time = Utility.toPersianNumber(time);
        return time;
    }

    public static void makeSnackBar(FragmentActivity fragmentActivity, String message){
        View rootView = fragmentActivity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar mysnack= Snackbar.make(rootView, message, Snackbar.LENGTH_LONG);
        View view= mysnack.getView();
        view.setBackgroundColor(Color.GRAY);
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
//        tv.setTextSize(fragmentActivity.getResources().getDimension(R.dimen.snack_size));
        tv.setTextSize(fragmentActivity.getResources().getInteger(R.integer.toast_size));
        Typeface typeface = adjustFont(fragmentActivity);
        tv.setTypeface(typeface);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.BOTTOM | Gravity.START;
        view.setLayoutParams(params);
        mysnack.show();
    }

    public static String addCommasToNumericString (String digits){
        String result = "";
        if (digits.contains("نامشخص"))
            return "قیمت مشخص نشده است";
        digits = digits.replace("\n","");
        int len = digits.length();
        int nDigits = 0;
        for (int i = len - 1; i >= 0; i--){
            result = digits.charAt(i) + result;
            nDigits++;
            if (((nDigits % 3) == 0) && (i > 0)){
                result = "," + result;
            }
        }
        return result;
    }

    public static long getDiff(Date one, Date two){
        long diff = one.getTime() - two.getTime();
        return diff;
    }

    public static void deviceInfo(FragmentActivity fragmentActivity, ArrayList<Pair<String, String>> args, String where){
        Preferences preferences = new Preferences(fragmentActivity);
        PackageInfo pInfo = null;
        String versionCode = "";
        String versionName = "";

        String tempDeviceId = preferences.getString("deviceID", "false");
        args.add(new Pair<>("deviceid", tempDeviceId));

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        try {
            pInfo = fragmentActivity.getPackageManager().getPackageInfo(fragmentActivity.getPackageName(), 0);
            versionCode = String.valueOf(pInfo.versionCode);
            versionName = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        args.add(new Pair<>("where", where));
        args.add(new Pair<>("appversioncode", versionCode));
        args.add(new Pair<>("appversionname", versionName));
        args.add(new Pair<>("phonemanufacturer", Build.MANUFACTURER));
        args.add(new Pair<>("phonebrand", Build.BRAND));
        args.add(new Pair<>("phonemodel", Build.MODEL));
        args.add(new Pair<>("api", String.valueOf(currentapiVersion)));
        DisplayMetrics displaymetrics = new DisplayMetrics();
        fragmentActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        args.add(new Pair<>("dim", String.valueOf(height) + " * " + String.valueOf(width)));
    }

    public static void longToDate(Long diff){
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);
        int diffInDays = (int) (diff / (1000 * 60 * 60 * 24));

        if (diffInDays > 1) {
            Log.e("Difference days (2) : ", String.valueOf(diffInDays));
            return ;
        } else if (diffHours > 0) {
            Log.e(">24", String.valueOf(diffHours));
            return ;
        } else if ((diffHours == 0) && (diffMinutes >= 1)) {
            Log.e("minutes", String.valueOf(diffMinutes));
            return ;
        }
    }

}
