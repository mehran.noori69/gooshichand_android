package com.gooshichand.Tools;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.gooshichand.R;


/**
 * Created by MaHkOoM on 12/18/2016.
 */
public class UpdateDialogFragment extends DialogFragment {

    private TextView body;
    public TextView detail;
    private TextView cancel;
    private TextView get;
    public String comment;
    public String link;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_update, container, false);
        Typeface myTypeface = Utility.adjustFont(getActivity());

        body = (TextView) rootView.findViewById(R.id.fragmentUpdateBody);
        detail = (TextView) rootView.findViewById(R.id.fragmentUpdateDetail);
        detail.setText(comment);
        cancel = (TextView) rootView.findViewById(R.id.cancelUpdate);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        get = (TextView) rootView.findViewById(R.id.getUpdate);
        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(link));
                getActivity().startActivity(i);
            }
        });

        body.setTypeface(myTypeface);
        detail.setTypeface(myTypeface);
        get.setTypeface(myTypeface);
        cancel.setTypeface(myTypeface);

        return rootView;
    }

}