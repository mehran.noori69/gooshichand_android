package com.gooshichand.Tools;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gooshichand.Models.MobileModels;

import java.util.concurrent.ExecutionException;


/**
 * Created by MaHkOoM021 on 4/30/2016.
 */
public class GlideImageLoader extends AsyncTask<String, Void, String> {
    public FragmentActivity fragmentActivity;
    public Fragment fragment;
    public MobileModels advertisementModel;
    public Bitmap img;
    public String url;
    public ImageView image;
    public Boolean isFully;

    public GlideImageLoader(FragmentActivity fragmentActivity, Fragment fragment, MobileModels advertisementModel, String url, ImageView image) {
        this.fragmentActivity = fragmentActivity;
        this.fragment = fragment;
        this.advertisementModel = advertisementModel;
        this.url = url;
        this.image = image;
        isFully = true;
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }

    protected String doInBackground(String... urls) {
//        Log.e("salam", url);
        if(isFully){ // for original img
            try {
                img = Glide.with(fragmentActivity).
                        load(url).
                        asBitmap().
                        diskCacheStrategy(DiskCacheStrategy.SOURCE).
//                        skipMemoryCache(true).
                        into(-1, -1). // Width and height
                        get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        else { // for thumbnails
            try {
                img = Glide.with(fragmentActivity).
                        load(url).
                        asBitmap().
                        diskCacheStrategy(DiskCacheStrategy.SOURCE ).
//                        skipMemoryCache(true).
                        into(200, 200). // Width and height
                        get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return "";
    }


    @Override
    protected void onPostExecute(String result) {
        if(isFully){ // for original img
            if(img!=null) {
                this.image.setImageBitmap(img);
//                if(fragment instanceof WorkshopViewFragment) {
//                    ((WorkshopViewFragment) fragment).recievedImg = img;
//                    ((WorkshopViewFragment) fragment).load.setVisibility(View.GONE);
//                }
            }
        }
    }

}
