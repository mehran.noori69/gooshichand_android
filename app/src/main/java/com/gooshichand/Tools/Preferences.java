package com.gooshichand.Tools;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;

/**
 * Created by MaHkOoM on 11/13/2015.
 */
public class Preferences{

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;

    public Preferences(FragmentActivity fragmentActivity){
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(fragmentActivity);
        editor = mSharedPreferences.edit();
    }

    public String getString(String name, String defaultValue){
        return mSharedPreferences.getString(name, defaultValue);
    }

    public void saveString(String name, String value){
        editor.putString(name, value);
        editor.apply();
    }

    public void removeString(String name){
        editor.remove(name);
        editor.apply();
    }

    public void clearPreferences(){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}