package com.gooshichand.Tools;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;

import com.gooshichand.R;

/**
 * Created by MaHkOoM021 on 2/25/2016.
 */
public class ServiceCalls {

    public FragmentActivity fragmentActivity;
    public String baseUrl ;
    public Boolean invisibleService = false;
    public Boolean failed = false;

    public void service(FragmentActivity fragmentActivity, HasServiceCall hasServiceCall, String serviceFunction, String serviceType, ArrayList<Pair<String, String>> args, String hint) {
        this.fragmentActivity = fragmentActivity;

        this.baseUrl = fragmentActivity.getString(R.string.serviceBaseURL);
        String serviceURL = this.baseUrl + serviceFunction ;

        JSONObject request = new JSONObject();

        if(serviceType.equalsIgnoreCase("POST")) {
            for (int i = 0; i < args.size(); i++) {
                try {
                    request.put(args.get(i).first, args.get(i).second);
                } catch (JSONException e) {
                    e.toString();
                }
            }
        }
        ServiceCall serviceCall = new ServiceCall(fragmentActivity, hasServiceCall, serviceType, serviceURL, request.toString(), hint);
        serviceCall.execute();
    }

    class ServiceCall extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;
        FragmentActivity fragmentActivity;
        HasServiceCall hasServiceCall;
        String hint;
        String serviceURL;
        String serviceType;
        String request;

        ServiceCall(FragmentActivity fragmentActivity, HasServiceCall hasServiceCall, String serviceType, String url, String request, String hint){
            this.fragmentActivity = fragmentActivity;
            this.hint = hint;
            this.hasServiceCall = hasServiceCall;
            progressDialog = new ProgressDialog(fragmentActivity);
            this.serviceURL = url;
            this.serviceType = serviceType;
            this.request = request;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            URL url;
            String result = "";
            try {
                url = new URL(serviceURL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(10000);
                conn.setRequestMethod(serviceType);
                conn.setDoInput(true);

                if(serviceType.equalsIgnoreCase("post")) {
                    conn.setDoOutput(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(request);
                    writer.flush();
                    writer.close();
                    os.close();
                }

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    InputStream gi = conn.getInputStream();
                    InputStreamReader isr = new InputStreamReader(gi);
                    BufferedReader br = new BufferedReader(isr);
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                }
                else {
                    result = fragmentActivity.getString(R.string.network_connectivity_failed);
                    Utility.makeSnackBar(fragmentActivity, result);
                    failed = true;
                }
            } catch (Exception e) {
                result = e.toString();
                Utility.makeSnackBar(fragmentActivity, fragmentActivity.getString(R.string.network_connectivity_error));
                failed = true;
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if (!failed)
                hasServiceCall.afterServiceCall(result, hint);
            else
                hasServiceCall.afterFailedServiceCall(result, hint);
        }
    }
}
