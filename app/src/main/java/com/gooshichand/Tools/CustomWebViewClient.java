package com.gooshichand.Tools;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by MaHkOoM on 2/10/2017.
 */

public class CustomWebViewClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}

