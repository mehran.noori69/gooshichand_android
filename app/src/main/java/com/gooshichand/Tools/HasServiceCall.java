package com.gooshichand.Tools;

/**
 * Created by User on 3/17/2018.
 */

public interface HasServiceCall {

    public abstract void afterServiceCall(String succeed, String hint);

    public abstract void afterFailedServiceCall(String failed, String hint);
}
